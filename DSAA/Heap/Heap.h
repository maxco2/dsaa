#pragma once
#include <initializer_list>
#include <stdexcept>
#include <vector>

template <typename T>
class Heap
{
private:
    std::vector<T> heap;

private:
    int parent(int cur) { return (cur - 1) / 2; }
    int leftChild(int cur) { return cur * 2 + 1; }
    int rightChild(int cur) { return cur * 2 + 2; }
    void shiftUp(int cur)
    {
        auto data = heap[cur];
        int parent = this->parent(cur);
        while (cur != 0)
            {
                parent = this->parent(cur);
                if (heap[parent] > data)
                    {
                        heap[cur] = heap[parent];
                        cur = parent;
                    }
                else
                    break;
            }
        heap[cur] = data;
    }
    void shiftDown(int cur)
    {
		if (heap.empty()) return;
        auto data = heap[cur];
        auto child = leftChild(cur);
        auto end = heap.size() - 1;
        while (child <= end)
            {
                // find lowest children
                if (child < end && heap[child] > heap[child + 1])
                    child = child + 1;
                // shift up
                if (data > heap[child])
                    {
                        heap[cur] = heap[child];
                        cur = child;
                        child = leftChild(cur);
                    }
                else
                    break;
            }
        heap[cur] = data;
    }

public:
    Heap() : heap() {}
    Heap(std::initializer_list<T> list)
    {
        heap = list;
        if (heap.empty()) return;
        int last = static_cast<int>(heap.size() - 1);
        last = parent(last);
        while (last >= 0)
            {
                shiftDown(last);
                last--;
            }
    }

    void insert(T data)
    {
        heap.emplace_back(data);
        shiftUp(static_cast<int>(heap.size() - 1));
    }

    T remove()
    {
        if (heap.empty()) throw std::runtime_error{"heap empty"};
        auto ret = heap.front();
        std::swap(heap.front(), heap.back());
        heap.pop_back();
        shiftDown(0);
        return ret;
    }

    bool size() { return heap.size(); }
    bool empty() { return heap.empty(); }

    std::vector<T> collect() { return heap; }
};
