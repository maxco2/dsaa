#include "DisjointSet.h"

int DisjointSet::naiveFind(int x)
{
    while (parent_[x] >= 0)
        x = parent_[x];
    return x;
}

void DisjointSet::naiveUnion(int root1, int root2)
{
	root1 = naiveFind(root1);
	root2 = naiveFind(root2);
	parent_[root1] += parent_[root2];
    parent_[root2] = root1;
}

void DisjointSet::weightedUnion(int root1, int root2)
{
	root1 = naiveFind(root1);
	root2 = naiveFind(root2);  
	if (root1 != root2) // root 1 is largr than root2
        {
		if (parent_[root1] < parent_[root2])
		{
			parent_[root1] += parent_[root2];
			parent_[root2] = root1;
		}
		else
		{
			parent_[root2] += parent_[root1];
			parent_[root1] = root2;
		}
        }
}

int DisjointSet::collapsingFind(int x)
{
    int root = naiveFind(x);
    int cur = x;
    while (cur!= root)
        {
			int curParent = parent_[cur];
            parent_[cur] = root;
            cur = curParent;
        }
    return root;
}
