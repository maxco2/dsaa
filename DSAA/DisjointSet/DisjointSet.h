#include <vector>

class DisjointSet
{
private:
    std::vector<int> parent_;

public:
    DisjointSet(std::size_t size) : parent_(size, -1) {}
    void naiveUnion(int root1, int root2);
    int naiveFind(int x);
    void weightedUnion(int root1, int root2);
    int collapsingFind(int x);
    std::vector<int> getDisjointSetVector() { return parent_; }
};
