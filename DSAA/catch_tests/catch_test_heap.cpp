#include "../Heap/Heap.h"
#include <iostream>
#include "catch.hpp"

template<typename T>
auto printHeap = [](Heap<T> heap)
{
	using namespace std;
	INFO("\nprint heap:\n");
	for (auto i : heap.collect())
		INFO(i << " ");
	INFO("\n");
};

TEST_CASE("min heap", "heap")
{
    {
        Heap<int> heap{9, 6, 1, 5, 2, 7, 10, 3, 4};
        auto collect = heap.collect();
        auto result = std::vector<int>{1, 2, 7, 3, 6, 9, 10, 5, 4};
        REQUIRE(std::equal(collect.begin(), collect.end(), result.begin()));
        printHeap<int>(heap); // should be 1,2,7,3,6,9,10,5,4
        heap.insert(0);
        collect = heap.collect();
        result = std::vector<int>{0, 1, 7, 3, 2, 9, 10, 5, 4, 6};
        REQUIRE(std::equal(collect.begin(), collect.end(), result.begin()));
        printHeap<int>(heap); // should be 0,1,7,3,2,9,10,5,4,6};
    }

    {
        Heap<int> heap{9, 2, 3, 1};
        auto collect = heap.collect();
        auto result = std::vector<int>{1, 2, 3, 9};
        REQUIRE(std::equal(collect.begin(), collect.end(), result.begin()));
        printHeap<int>(heap); // should be 1,2,3,9

        heap.insert(0);
        collect = heap.collect();
        result = std::vector<int>{0, 1, 3, 9, 2};
        REQUIRE(std::equal(collect.begin(), collect.end(), result.begin()));
        printHeap<int>(heap); // should be 0,1,3,9,2

        REQUIRE(heap.remove() == 0);
        result = std::vector<int>{1, 2, 3, 9};
        collect = heap.collect();
        REQUIRE(std::equal(collect.begin(), collect.end(), result.begin()));
    }
    {
        Heap<int> heap{};
        heap.insert(5);
        heap.insert(2);
        heap.insert(3);
        heap.insert(1);
        heap.insert(7);
		REQUIRE(heap.remove() == 1);
		REQUIRE(heap.remove() == 2);
		REQUIRE(heap.remove() == 3);
		REQUIRE(heap.remove() == 5);
		REQUIRE(heap.remove() == 7);
		try
		{
			heap.remove();
		}
		catch (const std::exception&)
		{
			REQUIRE(heap.collect().empty());
		}
    }
}
