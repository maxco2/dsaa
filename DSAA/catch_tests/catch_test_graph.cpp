#include "../Graph/Graph.h"
#include "catch.hpp"
#include <functional>
#include <set>
#include <unordered_set>
using namespace std;

// custom specialization of std::hash can be injected in namespace std
namespace std
{
	template <class T>
	inline void hash_combine(std::size_t &seed, const T &v)
	{
		std::hash<T> hasher;
		seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	}

	template <>
	struct hash<set<string>>
	{
		// use set to ensure string order  for which produces stable hash seed.
		typedef set<string> argument_type;
		typedef std::size_t result_type;
		result_type operator()(argument_type const &s) const noexcept
		{
			size_t seed = 0;
			for (const string &str : s)
			{
				hash_combine<string>(seed, str);
			}
			return seed;
		}
	};
} // namespace std

SCENARIO("Test graph utility operations", "[Graph]")
{
	// this is a test fixture for each section
	GIVEN("A directed graph with some vertices and edges")
	{
		Graph<int, int> g;
		g.insertVertex(0, "A");
		g.insertVertex(1, "B");
		g.insertVertex(2, "C");
		g.insertVertex(3, "D");
		g.insertVertex(4, "E");
		g.insertVertex(5, "G");
		g.insertVertex(6, "F");
		//		   +---------+         +---------+
		//		   |         v         v         |
		//		 +---+     +-------------+       |
		//		 | F | <-- |      A      | -+    |
		//		 +---+     +-------------+  |    |
		//		   |                   |    |    |
		//	  +----+                   |    |    |
		//	  |                        v    |    |
		//	  |  +---+     +---+     +---+  |    |
		//	  |  | G | <-- | E |  +- | B |  |    |
		//	  |  +---+     +---+  |  +---+  |    |
		//	  |    |         |    |    |    |    |
		//	  |    |         |    |    |    |    |
		//	  |    |         |    |    v    |    |
		//	  |    |         |    |  +---+  |    |
		//	  |    +---------+----+> | D | -+----+
		//	  |              |    |  +---+  |
		//	  |              |    |    |    |
		//	  |         +----+----+    |    |
		//	  |         |    |         v    v
		//	  |         |    |       +-------------+
		//	  |         |    +-----> |             |
		//	  |         |            |             |
		//	  |         |            |             |
		//	  |         +----------> |      C      |
		//	  |                      |             |
		//	  |                      |             |
		//	  +--------------------> |             |
		//							 +-------------+
		g.insertEdge("A", "B", 1);
		g.insertEdge("A", "F", 1);
		g.insertEdge("A", "C", 1);
		g.insertEdge("B", "C", 1);
		g.insertEdge("B", "D", 1);
		g.insertEdge("D", "A", 1);
		g.insertEdge("D", "C", 1);
		g.insertEdge("E", "C", 1);
		g.insertEdge("E", "G", 1);
		g.insertEdge("F", "A", 1);
		g.insertEdge("F", "C", 1);
		g.insertEdge("G", "D", 1);
		g.insertEdge("G", "E", 1);
		unordered_set<set<string>> sccKosaraju;
		// use set to ensure string order  for which produces stable hash seed.
		unordered_set<set<string>> sccTarjan;
		auto visit = [](const std::string &name) {
			INFO( name << " \n");
		};
		auto visitSCC = [](const std::vector<string> &vec,
			unordered_set<set<string>> &sccSet) {
			set<string> scc{ vec.begin(), vec.end() };
			INFO("strong connected compents:\n");
			for (auto &&name : vec)
				INFO(name << " ");
			INFO("\n");
			sccSet.emplace(scc);
		};
		auto visitEdgeStatus = [](std::string u, std::string v, std::string status) {INFO("Edge " << u << "->" << v << " status:" << status<<"\n"); };
		auto visitVertexTime = [](std::string vertex, int dTime, int fTime) {INFO("Vertex:" << vertex << " dTime:" << dTime << " fTime:" << fTime << " \n"); };
		WHEN("start BFS at A")
		{
			g.BFS(visit, "A");
			THEN("print all edge status:") { g.visitAllEdgeStatus(visitEdgeStatus); }
		}
		WHEN("start DFS at A")
		{
			g.DFS(visit, "A");
			THEN("print all edge status:") { g.visitAllEdgeStatus(visitEdgeStatus); }
			THEN("print all vertices time") { g.visitAllVertexTime(visitVertexTime); }
		}
		WHEN("compute strong connected components")
		{
			g.Kosaraju([&visitSCC, &sccKosaraju](const vector<string> &vec) {
				visitSCC(vec, sccKosaraju);
			});
			g.Tarjan([&visitSCC, &sccTarjan](const vector<string> &vec) {
				visitSCC(vec, sccTarjan);
			});
			THEN("compare Kosaraju and tarjan scc set")
			{
				REQUIRE(sccTarjan == sccKosaraju);
			}
		}
		WHEN("compute dominator")
		{
			Graph<int, int> g1;
			g1.insertVertex(0, "0");
			g1.insertVertex(1, "1");
			g1.insertVertex(2, "2");
			g1.insertVertex(3, "3");
			g1.insertVertex(4, "4");
			g1.insertVertex(5, "5");
			g1.insertVertex(6, "6");
			g1.insertVertex(7, "7");
			g1.insertVertex(8, "8");
			g1.insertVertex(9, "9");
			g1.insertUndirectedEdge(0, 5, 1);
			g1.insertEdge(0, 4, 1);
			g1.insertEdge(4, 3, 1);
			g1.insertEdge(3, 1, 1);
			g1.insertEdge(1, 0, 1);
			g1.insertEdge(2, 1, 1);
			g1.insertEdge(8, 2, 1);
			g1.insertEdge(9, 7, 1);
			g1.insertEdge(7, 8, 1);
			g1.insertEdge(7, 6, 1);
			g1.insertEdge(5, 7, 1);
			g1.insertEdge(6, 5, 1);
			auto ret1 = g1.computeDominator("7", "4");
			auto ret2 = g1.computeDominator("1", "7");
			auto ret3 = g1.computeDominator("7", "1");
			//todo
		}
	}
}

SCENARIO("Test biconnected graph", "[Graph]")
{
	GIVEN("An undirected graph")
	{
		Graph<int, int> g;
		g.insertVertex(0, "A");
		g.insertVertex(1, "B");
		g.insertVertex(2, "C");
		g.insertVertex(3, "D");
		g.insertVertex(4, "E");
		g.insertVertex(5, "F");
		g.insertVertex(6, "G");
		g.insertVertex(7, "H");
		g.insertVertex(8, "I");
		g.insertVertex(9, "J");
		//  +------------------------+
		//  |                        |
		//  |            +---+       |
		//  |         +- | E | ------+----+
		//  |         |  +---+       |    |
		//  |         |    |         |    |
		//  |         |    |         |    |
		//  |         |    |         |    |
		//+---+       |  +-------------+  |       +---+
		//| H |       |  |      F      |  |       | I |
		//+---+       |  +-------------+  |       +---+
		//            |    |    |    |    |         |
		//            |    |    |    +----+---------+
		//            |    |    |         |
		//            |  +---+  |         |
		//            +- | G |  +---------+----+
		//               +---+            |    |
		//                 |              |    |
		//                 |              |    |
		//                 |              |    |
		//               +-------------+  |    |
		//               |             | -+    |
		//               |      C      |       |
		//               |             |       |
		//       +------ |             |       |
		//       |       +-------------+       |
		//       |         |    |              |
		//       |         |    |              |
		//       |         |    |              |
		//     +---+     +---+  |              |
		//     | D | --- | A | -+----+         |
		//     +---+     +---+  |    |         |
		//       |         |    |    |         |
		//       |         |    |    |         |
		//       |         |    |    |         |
		//       |       +---+  |    |         |
		//       |       | B | -+    |         |
		//       |       +---+       |         |
		//       |                   |         |
		//       +-------------------+         |
		//                                     |
		//               +---+                 |
		//               | J | ----------------+
		//               +---+
		g.insertUndirectedEdge("A", "B", 1);
		g.insertUndirectedEdge("A", "D", 1);
		g.insertUndirectedEdge("A", "C", 1);
		g.insertUndirectedEdge("B", "C", 1);
		g.insertUndirectedEdge("D", "C", 1);
		g.insertUndirectedEdge("E", "C", 1);
		g.insertUndirectedEdge("E", "G", 1);
		g.insertUndirectedEdge("E", "F", 1);
		g.insertUndirectedEdge("F", "G", 1);
		g.insertUndirectedEdge("F", "H", 1);
		g.insertUndirectedEdge("F", "I", 1);
		g.insertUndirectedEdge("I", "H", 1);
		g.insertUndirectedEdge("I", "J", 1);
		g.insertUndirectedEdge("F", "J", 1);
		g.insertUndirectedEdge("G", "C", 1);
		WHEN("call tarjan")
		{
			std::set<string> apSet = { "F","C" };
			std::set<string> apSetCalc;
			std::map<string, string> bridges;
			std::unordered_set<std::set<string>> bcc = { {"C","E","F","G"},{"F","H","I","J"},{"A","B","C","D"} };
			std::unordered_set<std::set<string>> bccCalc; 
			auto visit = [&g, &apSet,&bccCalc](const vector<pair<string, string>> &name) {
				INFO("bcc:\n");
				set<string>  ccSet;
				for (auto&& uv : name)
				{
					INFO(uv.first << "---" << uv.second << " ");
					ccSet.insert({ uv.first,uv.second });
				}
				if (!ccSet.empty()) bccCalc.insert(ccSet);
				INFO("\n");
			};
			g.BCCTarjan(visit, apSetCalc, bridges, false/*calc biconnected components*/);
			REQUIRE(bridges.empty());
			REQUIRE(apSetCalc == apSet);
			REQUIRE(bcc == bccCalc);
		}
	}
	GIVEN("Another undirected graph")
	{
		Graph<int, int> g;
		g.insertVertex(0, "0");
		g.insertVertex(1, "1");
		g.insertVertex(2, "2");
		g.insertVertex(3, "3");
		g.insertVertex(4, "4");
		g.insertVertex(5, "5");
		g.insertVertex(6, "6");
		g.insertVertex(7, "7");
		g.insertVertex(8, "8");
		g.insertVertex(9, "9");
		g.insertUndirectedEdge(0, 1, 1);
		g.insertUndirectedEdge(1, 3, 1);
		g.insertUndirectedEdge(3, 6, 1);
		g.insertUndirectedEdge(3, 9, 1);
		g.insertUndirectedEdge(6, 9, 1);
		g.insertUndirectedEdge(6, 5, 1);
		g.insertUndirectedEdge(5, 8, 1);
		g.insertUndirectedEdge(5, 7, 1);
		g.insertUndirectedEdge(5, 2, 1);
		g.insertUndirectedEdge(2, 7, 1);
		g.insertUndirectedEdge(2, 4, 1);
		WHEN("call tarjan")
		{
			std::set<string> apSet = { "1","3","5","6","2" };
			std::set<string> apSetCalc;
			std::map<string, string> bridges = { {"0","1"},{"1","3"},{"5","8"},{"2","4"},{"6","5"} };
			std::map<string, string> calcBridges;
			std::unordered_set<std::set<string>> doubleEdgeCC = { {"0"},{"1"},{"3","6","9"},{"8"},{"2","5","7"},{"4"} };
			std::unordered_set<std::set<string>> doubleEdgeCCCalc;
			auto visit = [&g, &doubleEdgeCCCalc](const vector<pair<string, string>> &name) {
				INFO("2-edge-cc:\n");
				set<string>  ccSet;
				for (auto&& uv : name)
					if (uv.second.empty())
					{
						set<string> tmp = { uv.first };
						doubleEdgeCCCalc.insert(tmp);
						INFO(uv.first << "\n");
					}
					else
					{
						ccSet.insert({ uv.first,uv.second });
						INFO(uv.first << "---" << uv.second << " ");
					}
				if (!ccSet.empty()) doubleEdgeCCCalc.insert(ccSet);
				INFO("\n");
			};
			g.BCCTarjan(visit, apSetCalc, calcBridges, true/*calc 2-edge-connected components*/);
			REQUIRE(apSetCalc == apSet);
			REQUIRE(bridges == calcBridges);
			REQUIRE(doubleEdgeCC == doubleEdgeCCCalc);
		}
	}
}

SCENARIO("Test undirected graph minimum spanning tree", "[Graph]")
{
	GIVEN("An undirected graph")
	{
		Graph<int, int> g;
		g.insertVertex(0, "A");
		g.insertVertex(1, "B");
		g.insertVertex(2, "C");
		g.insertVertex(3, "D");
		g.insertVertex(4, "E");
		g.insertVertex(5, "F");
		g.insertVertex(6, "G");
		//  +----------+
		//  |          |
		//  |        +---+  5    +-----+
		//  |    +-- | D | ----- |  A  |
		//  |    |   +---+       +-----+
		//  |    |     |           |
		//  |    | 9   |           | 7
		//  |    |     |           |
		//  |    |     |         +-----+
		//  |    +-----+-------- |  B  | -+
		//  |          |         +-----+  |
		//  |          |           |      |
		//  |          |           | 8    |
		//  |          |           |      |
		//  |          |         +-----+  |
		//  |          |         |  C  |  |
		//  |          |         +-----+  |
		//  |          |           |      |
		//  |          |           | 5    | 7
		//  |          |           |      |
		//  |          |   15    +-----------+
		//  |          +-------- |     E     |
		//  |                    +-----------+
		//  |                      |      |
		//  |                      | 8    |
		//  |                      |      |
		//  |              6     +-----+  |
		//  +------------------- |  F  |  |
		//                       +-----+  |
		//                         |      | 9
		//                         | 11   |
		//                         |      |
		//                       +-----+  |
		//                       |  G  | -+
		//                       +-----+

		g.insertUndirectedEdge("A", "B", 7);
		g.insertUndirectedEdge("A", "D", 5);
		g.insertUndirectedEdge("B", "C", 8);
		g.insertUndirectedEdge("B", "D", 9);
		g.insertUndirectedEdge("B", "E", 7);
		g.insertUndirectedEdge("C", "E", 5);
		g.insertUndirectedEdge("D", "E", 15);
		g.insertUndirectedEdge("D", "F", 6);
		g.insertUndirectedEdge("E", "F", 8);
		g.insertUndirectedEdge("E", "G", 9);
		g.insertUndirectedEdge("F", "G", 11);
		WHEN("compute mst")
		{
			// https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Kruskal_Algorithm_6.svg/200px-Kruskal_Algorithm_6.svg.png
			unordered_multimap<string, string> mst = g.Kruskal();
			unordered_multimap<string, string> mst1 = g.Prim("A");
			unordered_multimap<string, string> expected{
				{"C", "E"}, {"A", "D"}, {"B", "E"},
				{"E", "G"}, {"D", "F"}, {"A", "B"},
			};
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			INFO("Kruskal result:\n");
			for (auto it = mst.begin(); it != mst.end(); ++it)
			{
				INFO(it->first << "<->" <<
					it->second << ",cost:" <<
					g.getWeight(g.getVertexByName(it->first),
						g.getVertexByName(it->second)) << "\n");
			}
			REQUIRE(mst == expected);
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			INFO("Prim result:\n");
			for (auto it = mst1.begin(); it != mst1.end(); ++it)
			{
				INFO(it->first << "<->" <<
					it->second << ",cost:" <<
					g.getWeight(g.getVertexByName(it->first),
						g.getVertexByName(it->second)) << "\n");
				
			}
			REQUIRE(mst1 == expected);
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
		}
	}
	GIVEN("another undirected graph")
	{
		Graph<int, int> g;
		g.insertVertex(0, "0");
		g.insertVertex(1, "1");
		g.insertVertex(2, "2");
		g.insertVertex(3, "3");
		g.insertVertex(4, "4");
		g.insertVertex(5, "5");
		g.insertVertex(6, "6");
		//                 +-----+
		//                 |  0  | -+
		//                 +-----+  |
		//                   |      |
		//                   | 28   |
		//                   |      |
		//     +---+  14   +-----+  |
		//+--- | 6 | ----- |  1  |  |
		//|    +---+       +-----+  |
		//|      |           |      |
		//|      |           | 16   |
		//|      |           |      |
		//|      |         +-----+  |
		//|      |         |  2  |  |
		//|      |         +-----+  |
		//|      |           |      |
		//| 24   |           | 12   | 10
		//|      |           |      |
		//|      |   18    +-----+  |
		//|      +---------|  3  |  |
		//|                +-----+  |
		//|                  |      |
		//|                  | 22   |
		//|                  |      |
		//|                +-----+  |
		//+--------------- |  4  |  |
		//                 +-----+  |
		//                   |      |
		//                   | 25   |
		//                   |      |
		//                 +-----+  |
		//                 |  5  | -+
		//                 +-----+
		g.insertUndirectedEdge(0, 1, 28);
		g.insertUndirectedEdge(0, 5, 10);
		g.insertUndirectedEdge(1, 2, 16);
		g.insertUndirectedEdge(1, 6, 14);
		g.insertUndirectedEdge(2, 3, 12);
		g.insertUndirectedEdge(3, 4, 22);
		g.insertUndirectedEdge(3, 6, 18);
		g.insertUndirectedEdge(4, 6, 24);
		g.insertUndirectedEdge(4, 5, 25);
		WHEN("compute mst")
		{
			unordered_multimap<string, string> mst = g.Kruskal();
			unordered_multimap<string, string> mst1 = g.Prim("0");
			unordered_multimap<string, string> expected{
				{"0", "5"}, {"4", "5"}, {"1", "6"},
				{"1", "2"}, {"2", "3"}, {"3", "4"},
			};
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			INFO("Kruskal result:\n");
			for (auto it = mst.begin(); it != mst.end(); ++it)
			{
				INFO(it->first << "<->" <<
					it->second << ",cost:" <<
					g.getWeight(g.getVertexByName(it->first),
						g.getVertexByName(it->second)) << "\n");
			}
			REQUIRE(mst == expected);
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			INFO("Prim result:\n");
			for (auto it = mst1.begin(); it != mst1.end(); ++it)
			{
				INFO( it->first<<"<->"<<
					it->second<<",cost:"<<
					g.getWeight(g.getVertexByName(it->first),
						g.getVertexByName(it->second))<<"\n");
			}
			REQUIRE(mst1 == expected);
			INFO("xxxxxxxxxxxxxxxxxxxxxxxxxx\n");
		}
	}
	GIVEN("An undirected graph test shortest path")
	{
		Graph<int, int> g;
		g.insertVertex(1, "1");
		g.insertVertex(2, "2");
		g.insertVertex(3, "3");
		g.insertVertex(4, "4");
		g.insertVertex(5, "5");
		g.insertVertex(6, "6");
		//            +-----+
		//       +--- |  1  | -+
		//       |    +-----+  |
		//       |      |      |
		//       |      | 7    |
		//       |      |      |
		//       |    +-----+  |
		//       |    |  2  | -+------+
		//       |    +-----+  |      |
		//       |      |      |      |
		//       |      | 10   | 9    |
		//       |      |      |      |
		//       |    +-----+  |      |
		//  +----+--- |  3  | -+      |
		//  |    |    +-----+         |
		//  |    |      |             |
		//  |    |      | 11          |
		//  |    |      |             |
		//  |    |    +-----+  15     |
		//  |    |    |  4  | --------+
		//  |    |    +-----+
		//  |    |      |
		//  |    | 14   | 6
		//  |    |      |
		//  |    |    +-----+
		//  |    |    |  5  |
		//  |    |    +-----+
		//  |    |      |
		//  |    |      | 9
		//  |    |      |
		//  |    |    +-----+
		//  |    +--- |  6  |
		//  |         +-----+
		//  |   2       |
		//  +-----------+
		g.insertUndirectedEdge("1", "2", 7);
		g.insertUndirectedEdge("1", "6", 14);
		g.insertUndirectedEdge("1", "3", 9);
		g.insertUndirectedEdge("2", "4", 15);
		g.insertUndirectedEdge("2", "3", 10);
		g.insertUndirectedEdge("3", "4", 11);
		g.insertUndirectedEdge("3", "6", 2);
		g.insertUndirectedEdge("4", "5", 6);
		g.insertUndirectedEdge("5", "6", 9);
		WHEN("call dijkstra")
		{
			auto ret = g.Dijkstra(g.getVertexByName("1"));
			auto testDistanceProp = [&ret, &g](const string &cur,
				const string &prior,
				int distance) {
				REQUIRE(ret[g.getVertexByName(cur)].prior ==
					g.getVertexByName(prior));
				REQUIRE(ret[g.getVertexByName(cur)].distance == distance);
			};
			testDistanceProp("2", "1", 7);
			testDistanceProp("3", "1", 9);
			testDistanceProp("4", "3", 20);
			testDistanceProp("5", "6", 20);
			testDistanceProp("6", "3", 11);
		}
		WHEN("call bellford")
		{
			bool hasNegativeCycle = false;
			auto ret = g.BellFord(g.getVertexByName("1"), hasNegativeCycle);
			auto testDistanceProp = [&ret, &g](const string &cur,
				const string &prior,
				int distance) {
				REQUIRE(ret[g.getVertexByName(cur)].prior ==
					g.getVertexByName(prior));
				REQUIRE(ret[g.getVertexByName(cur)].distance == distance);
			};
			REQUIRE(hasNegativeCycle == false);
			testDistanceProp("2", "1", 7);
			testDistanceProp("3", "1", 9);
			testDistanceProp("4", "3", 20);
			testDistanceProp("5", "6", 20);
			testDistanceProp("6", "3", 11);
		}
		WHEN("call floyd-warshall")
		{
			auto ret = g.FloydWarshall();
			auto testDistanceProp = [&ret, &g](const string &cur,
				const string &prior,
				int distance) {

				REQUIRE(ret[g.getVertexByName("1")][g.getVertexByName(cur)].prior ==
					g.getVertexByName(prior));
				REQUIRE(ret[g.getVertexByName("1")][g.getVertexByName(cur)].distance == distance);
			};
			testDistanceProp("2", "1", 7);
			testDistanceProp("3", "1", 9);
			testDistanceProp("4", "3", 20);
			testDistanceProp("5", "6", 20);
			testDistanceProp("6", "3", 11);
		}
	}
	GIVEN("A directed graph check negative cycle")
	{

		Graph<int, int> g;
		g.insertVertex(1, "s");
		g.insertVertex(2, "t");
		g.insertVertex(3, "x");
		g.insertVertex(4, "y");
		g.insertVertex(5, "z");
		WHEN("call bellford [no negativeCycle]")
		{
			//             +----+
			//         +-- | s  |
			//         |   +----+
			//         |     |
			//         |     | 6
			//         |     v
			//         |   +----------------+
			//  +------+-> |       t        |
			//  |      |   +----------------+
			//  |      |     |     |      |
			//  |      | 7   | 8   |      |
			//  |      |     v     |      |
			//  | -2   |   +----+  |      |
			//  |      +-> | y  | -+------+----+
			//  |          +----+  |      |    |
			//  |            |     |      |    |
			//  +------+     | 9   | -4   |    |
			//         |     v     |      |    |
			//         |   +----+  |      |    |
			//         |   | z  | <+      |    |
			//         |   +----+         |    |
			//         |     |            |    |
			//         |     | 7          |    |
			//         |     v            |    |
			//         |   +----+  5      |    |
			//         +-- | x  | <-------+    |
			//             +----+              |
			//               ^    -3           |
			//               +-----------------+
			g.insertEdge("s", "t", 6);
			g.insertEdge("s", "y", 7);
			g.insertEdge("t", "x", 5);
			g.insertEdge("x", "t", -2);
			g.insertEdge("t", "y", 8);
			g.insertEdge("t", "z", -4);
			g.insertEdge("y", "x", -3);
			g.insertEdge("y", "z", 9);
			g.insertEdge("z", "x", 7);
			bool hasNegativeCycle = false;
			auto ret = g.BellFord(g.getVertexByName("s"), hasNegativeCycle);
			REQUIRE(hasNegativeCycle == false);
		}
		WHEN("call bellford[has negativeCycle]")
		{
			//              2
			//          +----------+
			//          |          v
			//          |        +----+
			//          |    +-- | s  |
			//          |    |   +----+
			//          |    |     |
			//          |    |     | 6
			//          |    |     v
			//          |    |   +----------------+
			//   +------+----+-> |       t        |
			//   |      |    |   +----------------+
			//   |      |    |     |     |      |
			//   |      |    | 7   | 8   |      |
			//   |      |    |     v     |      |
			//   |      |    |   +----+  |      |
			//   |      |    +-> | y  | -+------+----+
			//   |      |        +----+  |      |    |
			//   |      |          |     |      |    |
			//   | -3   |          | 9   | -4   |    |
			//   |      |          v     |      |    |
			//   |      |        +----+  |      |    |
			//   |      +------- | z  | <+      |    |
			//   |               +----+         |    |
			//   |                 |            |    |
			//   |                 | 7          |    |
			//   |                 v            |    |
			//   |               +----+  5      |    |
			//   +-------------- | x  | <-------+    |
			//                   +----+              |
			//                     ^    -3           |
			//                     +-----------------+
			g.insertEdge("s", "t", 6);
			g.insertEdge("s", "y", 7);
			g.insertEdge("t", "x", 5);
			g.insertEdge("x", "t", -3);
			g.insertEdge("t", "y", 8);
			g.insertEdge("t", "z", -4);
			g.insertEdge("y", "x", -3);
			g.insertEdge("y", "z", 9);
			g.insertEdge("z", "x", 7);
			g.insertEdge("z", "s", 2);
			bool hasNegativeCycle = false;
			auto ret = g.BellFord(g.getVertexByName("s"), hasNegativeCycle);
			REQUIRE(hasNegativeCycle == true);
		}
	}
}
