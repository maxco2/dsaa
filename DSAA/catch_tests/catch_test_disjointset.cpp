#include "../DisjointSet/DisjointSet.h"
#include "catch.hpp"
#include <stdio.h>

SCENARIO("Test Disjoint set", "[DisjointSet]")
{

    GIVEN("A disjoint set with ten elements")
    {
        DisjointSet s{10};
        // bootom up build union set
        s.naiveUnion(9, 3);
        s.naiveUnion(9, 5);
        s.naiveUnion(7, 9);
        s.naiveUnion(7, 1);
        s.naiveUnion(0, 6);
        s.naiveUnion(0, 7);
        s.naiveUnion(0, 8);
        WHEN("test union naive find")
        {
            REQUIRE(s.naiveFind(6) == 0);
            REQUIRE(s.naiveFind(7) == 0);
            REQUIRE(s.naiveFind(8) == 0);
            REQUIRE(s.naiveFind(1) == 0);
            REQUIRE(s.naiveFind(9) == 0);
            REQUIRE(s.naiveFind(3) == 0);
            REQUIRE(s.naiveFind(5) == 0);
        }
        WHEN("test collapsing find")
        {
            REQUIRE(s.collapsingFind(6) == 0);
            REQUIRE(s.collapsingFind(7) == 0);
            REQUIRE(s.collapsingFind(8) == 0);
            REQUIRE(s.collapsingFind(1) == 0);
            REQUIRE(s.collapsingFind(9) == 0);
            REQUIRE(s.collapsingFind(3) == 0);
            REQUIRE(s.collapsingFind(5) == 0);
        }
        WHEN("try collapsingFind 5")
        {
            s.collapsingFind(5);
            std::vector<int> vec = s.getDisjointSetVector();
            REQUIRE(vec[0] == -8);
            REQUIRE(vec[6] == 0);
            REQUIRE(vec[7] == 0);
            REQUIRE(vec[8] == 0);
            REQUIRE(vec[9] == 0);
            REQUIRE(vec[5] == 0);
            REQUIRE(vec[1] == 7);
            REQUIRE(vec[3] == 9);
        }
		WHEN("Test weight union")
		{
			s.weightedUnion(2, 0);
			REQUIRE(s.collapsingFind(2) == 0);
			s.weightedUnion(4, 2);
			REQUIRE(s.collapsingFind(4) == 0);
		}
    }
}

