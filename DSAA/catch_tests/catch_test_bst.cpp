#include "../BinaryTree/BinaryTree.h"
#include "catch.hpp"
#include <initializer_list>
using namespace std;

TEST_CASE("BST", "[BinaryTreeNoAVL]")
{
    initializer_list<int> list{9, 6, 10, 1, 7, 5, 2};
    BinaryTree<int> tree1{list};
    BinaryTree<int> tree2;
    for (auto i : list)
        {
            tree2.insertNoAVL(i);
        }
	auto visit = [](int i) {INFO(i << "\n"); };
    INFO("Traverse tree1:");
    tree1.lev_order_traversal(visit);
    INFO("\n\n");
    INFO("Traverse tree2:");
    tree2.lev_order_traversal(visit);
    INFO("\n\n");
    auto print = [](int i) { INFO(i << " ");};
    INFO("\nTree preorder no rec:\n");
    tree1.preOrderNoRec(print);
    INFO("\nTree preorder:\n");
    tree1.preOrder(print);
    INFO("\nTree inorder no rec:\n");
    tree1.inOrderNoRec(print);
    INFO("\nTree inorder:\n");
    tree1.inOrder(print);
    INFO("\nTree postorder no rec:\n");
    tree1.postOrderNoRec(print);
    INFO("\nTree postorder:\n");
    tree1.postOrder(print);
    INFO("\n\n");

    REQUIRE(tree1.parent(tree1.find(9)) == nullptr);
    REQUIRE(tree1.parent(tree1.find(6)) == tree1.find(9));
    REQUIRE(tree1.parent(tree1.find(10)) == tree1.find(9));
    REQUIRE(tree1.parent(tree1.find(7)) == tree1.find(6));
    REQUIRE(tree1.parent(tree1.find(1)) == tree1.find(6));
    REQUIRE(tree1.parent(tree1.find(5)) == tree1.find(1));
    REQUIRE(tree1.parent(tree1.find(2)) == tree1.find(5));

    INFO("create thread:\n");
    tree1.createInThread();
    INFO("\nTree inorder:\n");
    tree1.inOrderThreaded(print);
}
