﻿cmake_minimum_required (VERSION 3.0)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_BUILD_TYPE Debug)

project ("DSAA")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
enable_testing()                                                                          #
message("${CMAKE_SOURCE_DIR}/cmake/")
set(CATCH_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Catch2)                     
include_directories(${INCLUDE_DIRECTORIES} ${CATCH_INCLUDE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})

macro(print_all_variables)
    message(STATUS "print_all_variables------------------------------------------{")
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
        message(STATUS "${_variableName}=${${_variableName}}")
    endforeach()
    message(STATUS "print_all_variables------------------------------------------}")
endmacro()

file(GLOB SOURCE_FILES "*.cpp"
"${CMAKE_CURRENT_SOURCE_DIR}/catch_tests/*.cpp"
"${CMAKE_CURRENT_SOURCE_DIR}/DisjointSet/*.cpp"
)    
  
message(${SOURCE_FILES})#
add_executable(${PROJECT_NAME} ${SOURCE_FILES}) 
include(CTest)
include(ParseAndAddCatchTests) 
ParseAndAddCatchTests(${PROJECT_NAME})   


