#pragma once
#include "Graph.h"

template <typename T, typename E>
void Graph<T, E>::DFS(std::function<void(const std::string &)> func,
                      const std::string &name,
                      std::function<void(int)> postOrder)
{
    int pos = getVertexByName(name);
    if (pos == -1) return;
    DFS(func, pos, postOrder);
}

template <typename T, typename E>
void Graph<T, E>::DFS(std::function<void(const std::string &)> func, int pos,
                      std::function<void(int)> postOrder)
{
    if (pos == -1) return;
    for (auto &&i : vertices_)
        {
            i.resetAllProp();
        }
    auto time = 0;
    DFSOneNode(func, pos, time, postOrder);
    for (auto i = 0; i != vertices_.size(); ++i)
        {
            if (vertices_[i].color == VertexColor::WHITE)
                DFSOneNode(func, i, time, postOrder);
        }
}

template <typename T, typename E>
void Graph<T, E>::DFSOneNode(std::function<void(const std::string &)> preOrder,
                             int pos, int &time,
                             std::function<void(int)> postOrder)
{
    time++;
    Vertex<T, E> &u = vertices_[pos];
    u.dTime = time;
    u.color = VertexColor::GRAY;
    if (preOrder) preOrder(u.name);
    for (Edge<E> *link = u.link; link != nullptr; link = link->link)
        {
            Vertex<T, E> &v = vertices_[link->dest];
            switch (v.color)
                {
                case VertexColor::WHITE:
                    v.parent = pos;
                    link->edgeStatus_ = EdgeStatus::TE;
                    DFSOneNode(preOrder, link->dest, time, postOrder);
                    break;
                case VertexColor::GRAY:
                    link->edgeStatus_ = EdgeStatus::BE;
                    break;
                case VertexColor::BLACK:
                    link->edgeStatus_ =
                        v.dTime < u.dTime ? EdgeStatus::CE : EdgeStatus::FE;
                    break;
                }
        }
    u.color = VertexColor::BLACK;
    time++;
    u.fTime = time;
    if (postOrder) postOrder(pos);
}
