#pragma once
#include "Graph.h"

template <typename T, typename E>
void Graph<T, E>::Tarjan(
    std::function<void(const std::vector<std::string> &)> visitSCC)
{
    size_t vertexSize = vertices_.size();
    std::unique_ptr<int[]> dfn{new int[vertexSize]};
    std::unique_ptr<int[]> low{new int[vertexSize]};
    std::unique_ptr<bool[]> stackMember{new bool[vertexSize]};
    for (size_t i = 0; i < vertexSize; ++i)
        {
            dfn[i] = low[i] = -1;
            stackMember[i] = false;
        }
    int time = 0;
    std::stack<int> stack;
    for (int vertex = 0; vertex < vertexSize; ++vertex)
        {
            if (dfn[vertex] == -1)
                TarjanRec(visitSCC, vertex, time, dfn.get(), low.get(), stack,
                          stackMember.get());
        }
}

template <typename T, typename E>
void Graph<T, E>::TarjanRec(
    const std::function<void(const std::vector<std::string> &)> &visitSCC,
    int vertex, int &time, int *dfn, int *low, std::stack<int> &stack,
    bool *stackMember)
{
    dfn[vertex] = low[vertex] = ++time;
    stack.push(vertex);
    stackMember[vertex] = true;
    Edge<E> *link = vertices_[vertex].link;
    while (link != nullptr)
        {
            if (dfn[link->dest] == -1)
                {
                    // not visited
				TarjanRec(visitSCC, link->dest, time, dfn, low, stack,
                              stackMember);
                    low[vertex] = std::min(low[link->dest], low[vertex]);
                  
                }
            else if (stackMember[link->dest])
                {
				low[vertex] = std::min(dfn[link->dest], low[vertex]);
                }
            link = link->link;
        }
    if (dfn[vertex] == low[vertex])
        {
            int v = -1;
            std::vector<std::string> scc;
            do
                {
                    v = stack.top();
                    stack.pop();
                    stackMember[v] = false;
                    scc.emplace_back(vertices_[v].name);
                }
            while (vertex != v);
            visitSCC(scc);
        }
}
