#pragma once
#include "Graph.h"
template <typename T, typename E>
void Graph<T, E>::BFS(std::function<void(const std::string &)> func,
                      const std::string &name)
{
    int pos = getVertexByName(name);
    if (pos == -1) return;
    for (auto &&i : vertices_)
        {
            i.resetAllProp();
        }
    BFSOneNode(func, pos); // start BFS at pos
    for (auto i = 0; i != vertices_.size(); ++i)
        {
            if (vertices_[i].color == VertexColor::WHITE) BFSOneNode(func, i);
        }
}

template <typename T, typename E>
void Graph<T, E>::BFSOneNode(std::function<void(const std::string &)> func,
                             int pos)
{
    std::queue<int> queue;
    queue.push(pos);
    vertices_[pos].color = VertexColor::GRAY;
    func(vertices_[pos].name);
    while (!queue.empty())
        {
            Vertex<T, E> u = vertices_[queue.front()];
            int upos = queue.front();
            queue.pop();
            for (auto link = u.link; link != nullptr; link = link->link)
                {
                    auto &v = vertices_[link->dest];
                    if (v.color == VertexColor::WHITE)
                        {
                            func(v.name);
                            v.color = VertexColor::GRAY;
                            v.parent = upos;
                            queue.push(link->dest);
                        }
                }
            u.color = VertexColor::BLACK;
        }
}
