#pragma once
#include "../DisjointSet/DisjointSet.h"
#include "../Heap/Heap.h"
#include "Graph.h"
#include <unordered_map>

template <typename T, typename E>
std::unordered_multimap<std::string, std::string> Graph<T, E>::Kruskal()
{
    std::unordered_multimap<std::string, std::string> ret;
    struct SimpleEdge
    {
        int start = -1;
        int end = -1;
        E cost{0};
        bool operator<(const SimpleEdge &that) { return cost < that.cost; }
        bool operator>(const SimpleEdge &that) { return cost > that.cost; }
    };
    size_t vertexSize = vertices_.size();
    DisjointSet ds{vertexSize};
    Heap<SimpleEdge> heap{};
    for (auto i = 0; i < vertices_.size(); ++i)
        {
            Edge<E> *edge = vertices_[i].link;
            while (edge != nullptr)
                {
                    if (edge->dest > i)
                        {
                            heap.insert(SimpleEdge{i, edge->dest, edge->cost});
                            // only insert start < end edge to avoid insert edge
                            // which end < start
                        }
                    edge = edge->link;
                }
        }
    while (!heap.empty())
        {
            SimpleEdge edge = heap.remove();
            if (ds.collapsingFind(edge.start) != ds.collapsingFind(edge.end))
                {
                    ret.emplace(vertices_[edge.start].name,
                                vertices_[edge.end].name);
                    ds.weightedUnion(edge.start, edge.end);
                }
        }
    return ret;
}
