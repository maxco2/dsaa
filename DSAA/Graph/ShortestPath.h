#pragma once
#include "../Heap/Heap.h"
#include "Graph.h"
#include <algorithm>

template <class T, class E>
inline std::map<int, typename Graph<T, E>::DistanceProp>
Graph<T, E>::Dijkstra(int vertex)
{
	if (vertex == -1) return {};
	auto distanceMap = initializeSingleSource(vertex);
	std::unique_ptr<bool[]> source{ new bool[vertices_.size()] };
	for (int i = 0; i < vertices_.size(); ++i)
		source[i] = false;
	Heap<DistanceProp> heap{};
	heap.insert(distanceMap[vertex]);
	while (!heap.empty())
	{
		DistanceProp dp = heap.remove();
		source[dp.vertex] = true;
		Edge<E> *edge = vertices_[dp.vertex].link;
		while (edge != nullptr)
		{
			if (!source[edge->dest] &&
				distanceMap[dp.vertex].distance + edge->cost <
				distanceMap[edge->dest].distance)
			{
				distanceMap[edge->dest].distance =
					distanceMap[dp.vertex].distance + edge->cost;
				distanceMap[edge->dest].prior = dp.vertex;
				heap.insert(distanceMap[edge->dest]);
			}
			edge = edge->link;
		}
	}
	return distanceMap;
}

template <class T, class E>
inline std::map<int, typename Graph<T, E>::DistanceProp>
Graph<T, E>::BellFord(int vertex, bool &negativeCycle)
{
	std::map<int, typename Graph<T, E>::DistanceProp> distanceMap =
		initializeSingleSource(vertex);
	Edge<E> *edge = vertices_[vertex].link;
	while (edge != nullptr)
	{
		distanceMap[edge->dest].distance = edge->cost;
		distanceMap[edge->dest].prior = vertex;
		edge = edge->link;
	}
	for (int j = 0; j < vertices_.size() - 2; ++j)
		for (int vPos = 0; vPos < vertices_.size(); ++vPos)
		{
			edge = vertices_[vPos].link;
			while (edge != nullptr)
			{
				if (distanceMap[edge->dest].distance >
					distanceMap[vPos].distance + edge->cost)
				{
					distanceMap[edge->dest].distance =
						edge->cost + distanceMap[vPos].distance;
					distanceMap[edge->dest].prior = vPos;
				}
				edge = edge->link;
			}
		}
	for (int  vPos = 0; vPos < vertices_.size(); ++vPos)
	{
		edge = vertices_[vPos].link;
		while (edge != nullptr)
		{
			if (distanceMap[edge->dest].distance >
				distanceMap[vPos].distance + edge->cost)
			{
				negativeCycle = true;
			}
			edge = edge->link;
		}
	}
	return distanceMap;
}

template <class T, class E>
inline std::map<int, std::map<int, typename Graph<T, E>::DistanceProp>>
Graph<T, E>::FloydWarshall()
{
	std::map<int, std::map<int, typename Graph<T, E>::DistanceProp>> distanceMap;
	for (auto i = 0; i < vertices_.size(); ++i)
	{
		auto vertexDistancemap = initializeSingleSource(i);
		Edge<E>* edge = vertices_[i].link;
		while (edge!= nullptr)
		{
			vertexDistancemap[edge->dest].prior = i;
			vertexDistancemap[edge->dest].distance = edge->cost;
			edge = edge->link;
		}
		distanceMap.emplace(i, vertexDistancemap);
	}//initilize prior matrix & distance map
	auto mapDistance = [&distanceMap](int start, int end)->int& {return distanceMap[start][end].distance; };
	auto mapPrior = [&distanceMap](int start, int end)->int& {return distanceMap[start][end].prior; };
	for (auto k = 0; k < vertices_.size(); ++k)
	{
		for (auto i = 0; i < vertices_.size(); ++i)
			for (auto j = 0; j < vertices_.size(); ++j)
			{
				Edge<E>* edge = vertices_[i].link;
				while (edge!= nullptr)
				{
					//interger overflow
					// i,j equals max() but i,k and k,j not eauals max, then assign new value
					// i,j equals max() and i,k or k,j equals max,then give up.
					// i,j not equals max, and i,k \  k,j not equals max,then assign new value
					// others give up.
					if (mapDistance(i, k) != std::numeric_limits<int>::max() &&
						mapDistance(k, j) != std::numeric_limits<int>::max())
						if (mapDistance(i, j) > mapDistance(i, k) + mapDistance(k, j))
						{
							//indicate the vertex k is one of which is i->j shortest path vertex;
							//so the prior(i,j) turn into prior(k,j)
							mapDistance(i, j) = mapDistance(i, k) + mapDistance(k, j);
							mapPrior(i, j) = mapPrior(k, j);
						}
					edge = edge->link;
				}
			}
	}
	return distanceMap;
}

template <class T, class E>
inline std::vector<std::string> Graph<T, E>::topologySorting()
{
	if (vertices_.empty()) return {};
	std::vector<std::string> topologyOrder{};
	DFS({}, vertices_[0].name, [this, &topologyOrder](int vPos) {
		topologyOrder.emplace(vertices_[vPos].name);
	});
	std::reverse(topologyOrder.begin(), topologyOrder.end());
	return topologyOrder;
}

