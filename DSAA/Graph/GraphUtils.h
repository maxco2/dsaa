#pragma once
#include "Graph.h"

template <typename T, typename E>
void Graph<T, E>::insertEdge(const std::string &start, const std::string &end,
                             E weight)
{
    int pos1 = getVertexByName(start);
    int pos2 = getVertexByName(end);
    insertEdge(pos1, pos2, weight);
}

template <typename T, typename E>
void Graph<T, E>::insertUndirectedEdge(const std::string &start,
                                       const std::string &end, E weight)
{
    int pos1 = getVertexByName(start);
    int pos2 = getVertexByName(end);
    insertUndirectedEdge(pos1, pos2, weight);
}

template <typename T, typename E>
void Graph<T, E>::visitAllVertexTime(std::function<void(std::string,int,int)> visit)
{
    for (int i = 0; i < vertices_.size(); i++)
        {
            Vertex<T, E> &p = vertices_[i];
			visit(p.name, p.dTime, p.fTime);
        }
}

template <typename T, typename E>
void Graph<T, E>::visitAllEdgeStatus(std::function<void(std::string, std::string, std::string)> visit)
{
    auto visitEdgeStatus = [this,&visit](int u, Edge<E> *edge) {
        auto v = edge->dest;
        std::string uname = vertices_[u].name;
        std::string vname = vertices_[v].name;
        std::string status = "Unknown";
        switch (edge->edgeStatus_)
            {
            case EdgeStatus::BE:
                status = "back edge";
                break;
            case EdgeStatus::TE:
                status = "tree edge";
                break;
            case EdgeStatus::CE:
                status = "cross edge";
                break;
            case EdgeStatus::FE:
                status = "forward edge";
                break;
            default:
                break;
            }
		visit(uname, vname, status);
    };
    for (int i = 0; i < vertices_.size(); i++)
        {
            Edge<E> *p = vertices_[i].link;
            while (p != nullptr)
                {
                    visitEdgeStatus(i, p);
                    p = p->link;
                }
        }
}

template <typename T, typename E>
T Graph<T, E>::getValue(int i)
{
    if (i >= 0)
        if (i < vertices_.size())
            {
                return vertices_[i].data;
            }
    throw std::runtime_error{"couldn't found target vertex"};
}

template <typename T, typename E>
E Graph<T, E>::getWeight(int n1, int n2)
{
    if (n1 >= 0 && n2 >= 0 && n1 < vertices_.size() && n2 < vertices_.size())
        {
            return vertices_[n1].getWeight(n2);
        }
    return std::numeric_limits<E>::max();
}

template <typename T, typename E>
int Graph<T, E>::getFirstNeighbor(int vertexPos)
{
    if (vertexPos != -1)
        {
            Edge<E> *p = vertices_[vertexPos].link;
            if (p) return p->dest;
        }
    return -1;
}

template <typename T, typename E>
int Graph<T, E>::getNextNeighbor(int v, int w)
{
    if (v != -1)
        {
            Edge<E> *p = vertices_[v].link;
            while (p != nullptr || p->dest != w)
                p = p->link_;
            if (p != nullptr && p->link_ != nullptr) return p->link_->dest_;
        }
    return -1;
}

template <typename T, typename E>
int Graph<T, E>::getVertexByName(const std::string &name)
{
    // printf("try find name:%s\n",name.data());
    if (namePos_.find(name) != namePos_.end())
        {
            return namePos_[name];
        }
    return -1;
}

template <typename T, typename E>
void Graph<T, E>::insertEdge(int v, int w, E weight)
{
    // printf("try insert Edge %d %d\n",v,w);
    if (w < 0 || w > vertices_.size())
        throw std::runtime_error{"wrong vertex index"};
    if (v < 0 || v > vertices_.size())
        throw std::runtime_error{"wrong vertex index"};
    Edge<E> *p = vertices_[v].link;
    if (p == nullptr)
        {
            p = new Edge<E>{w, weight};
            vertices_[v].link = p;
            return;
        }
    while (p->link != nullptr)
        p = p->link;
    p->link = new Edge<E>{w, weight};
}

template <typename T, typename E>
void Graph<T, E>::insertUndirectedEdge(int v, int w, E weight)
{
    insertEdge(v, w, weight);
    insertEdge(w, v, weight);
}

template <typename T, typename E>
int Graph<T, E>::insertVertex(T data, const std::string &name)
{
    auto pos = getVertexByName(name);
    if (pos == -1)
        {
            vertices_.push_back(Vertex<T, E>{data, name});
            namePos_.emplace(
                std::pair<std::string, int>{name, vertices_.size() - 1});
            return static_cast<int>(vertices_.size() - 1);
        }
    return pos;
}

template <typename T, typename E>
Graph<T, E> *Graph<T, E>::reverse()
{
    Graph *g = new Graph{};
    g->vertices_ = vertices_;
    g->namePos_ = namePos_;
    for (auto &&v : g->vertices_)
        {
            v.link = nullptr;
            v.resetAllProp();
        }
    for (int i = 0; i < vertices_.size(); ++i)
        {
            Edge<E> *p = vertices_[i].link;
            while (p != nullptr)
                {
                    g->insertEdge(p->dest, i, p->cost);
                    p = p->link;
                }
        }
    return g;
}

template <typename T, typename E>
Graph<T, E>::~Graph()
{
    for (auto &&i : vertices_)
        {
            deleteVertex(i);
        }
}

template <typename T, typename E>
void Graph<T, E>::deleteVertex(Vertex<T, E> &vertex)
{
    std::stack<Edge<T> *> stack;
    auto p = vertex.link;
    while (p != nullptr)
        {
            stack.push(p);
            p = p->link;
        }
}

template <class T, class E>
inline std::map<int, typename Graph<T, E>::DistanceProp>
Graph<T, E>::initializeSingleSource(int vertex)
{
    std::map<int, typename Graph<T, E>::DistanceProp> ret{};
    for (auto i = 0; i < vertices_.size(); ++i)
        ret.emplace(i, DistanceProp{i});
    ret[vertex].distance = 0;
    ret[vertex].prior = -1;
    return ret;
}

template<class T, class E>
inline std::unordered_set<std::string> Graph<T, E>::computeDominator(const std::string & start, const std::string & end)
{
	auto pos1 = getVertexByName(start);
	auto pos2 = getVertexByName(end);

	if (pos1 == -1 || pos2 == -1) return {};
	if (pos1==pos2) return { start };
	std::unordered_set<std::string> result;
	return result;
}
