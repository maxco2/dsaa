#pragma once
#include <functional>
#include <limits>
#include <map>
#include <memory>
#include <queue>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <set>

enum class EdgeStatus
{
    TE,
    BE,
    FE /*DE*/,
    CE,
    UNKNOWN
};

template <typename E>
struct Edge
{
    EdgeStatus edgeStatus_ = EdgeStatus::UNKNOWN;
    int dest;
    E cost;
    Edge<E> *link = nullptr;
    Edge(int num, E weight) : dest(num), cost(weight) {}
};

enum class VertexColor
{
    GRAY,
    WHITE,
    BLACK
};

template <typename T, typename E>
struct Vertex
{
    VertexColor color = VertexColor::WHITE;
    int dTime = -1; // detect time
    int fTime = -1; // finish time
    int parent = -1;
    int priority = -1;
    std::string name;
    Edge<E> *link = nullptr;
    T data;
    Vertex(T data, std::string name = "") : data(data), name(name) {}

    E getWeight(int dest)
    {
        Edge<E> *next = link;
        while (next != nullptr)
            {
                if (next->dest == dest)
                    {
                        return next->cost;
                    }
                next = next->link;
            }
        return std::numeric_limits<E>::max();
    }

    void resetColor() { color = VertexColor::WHITE; }
    void resetParent() { parent = -1; }
    void resetVisitTime() { dTime = fTime = -1; }
    void resetPriority() { priority = -1; }
    void resetAllProp()
    {
        resetColor();
        resetVisitTime();
        resetPriority();
    }
};

template <class T, class E>
class Graph
{
public:
    struct DistanceProp
    {
        int prior = -1;
        int distance = std::numeric_limits<int>::max();
        int vertex;
        DistanceProp(int vertex) : vertex(vertex) {}
        DistanceProp() : vertex(-1) {}
        bool operator<(const DistanceProp &that)
        {
            return distance < that.distance;
        }
        bool operator>(const DistanceProp &that)
        {
            return distance > that.distance;
        }
    };

private:
    std::map<std::string, int> namePos_;
    std::vector<typename Vertex<T, E>> vertices_;

private:
    void deleteVertex(Vertex<T, E> &vertex);
    std::map<int, DistanceProp> initializeSingleSource(int vertex);

public:
    Graph() {}
    ~Graph();
    Graph *reverse();

    void Tarjan(std::function<void(const std::vector<std::string> &)> visitSCC);

    void TarjanRec(
        const std::function<void(const std::vector<std::string> &)> &visitSCC,
        int vertex, int &time, int *dfn, int *low, std::stack<int> &stack,
        bool *stackMember);

    void
    Kosaraju(std::function<void(const std::vector<std::string> &)> visitSCC);

    T getValue(int i);

    E getWeight(int n1, int n2);

    int getFirstNeighbor(int vertexPos);

    int getNextNeighbor(int v, int w);

    int getVertexByName(const std::string &name);

    void insertEdge(int v, int w, E weight);

    void insertUndirectedEdge(int v, int w, E weight);

    int insertVertex(T data, const std::string &name);

    void DFS(std::function<void(const std::string &)> func,
             const std::string &name,
             std::function<void(int)> postOrder = std::function<void(int)>{});

    void DFS(std::function<void(const std::string &)> func, int pos,
             std::function<void(int)> postOrder = std::function<void(int)>{});

    void
    DFSOneNode(std::function<void(const std::string &)> preOrder, int pos,
               int &time,
               std::function<void(int)> postOrder = std::function<void(int)>{});

    void BFS(std::function<void(const std::string &)> func,
             const std::string &name);

    void BFSOneNode(std::function<void(const std::string &)> func, int pos);

    void insertEdge(const std::string &start, const std::string &end, E weight);

    void insertUndirectedEdge(const std::string &start, const std::string &end,
                              E weight);

	void visitAllVertexTime(std::function<void(std::string, int, int)> visit);

	void visitAllEdgeStatus(std::function<void(std::string, std::string, std::string)> visit);


    std::unordered_multimap<std::string, std::string> Kruskal();

    std::unordered_multimap<std::string, std::string>
    Prim(const std::string &startVertex);

    std::map<int, DistanceProp> Dijkstra(int vertex);

    std::map<int, DistanceProp> BellFord(int vertex, bool &negativeCycle);

    std::map<int, std::map<int, DistanceProp>>
    FloydWarshall();

    std::vector<std::string> topologySorting();

	std::unordered_set<std::string> computeDominator(const std::string& start, const std::string& end);
	
	void BCCTarjan(std::function<void(const std::vector<std::pair<std::string, std::string>>&)> visitBCC, std::set<std::string>& articulationPoints, std::map<std::string, std::string>& bridges, bool is2EdgeConnected);

	void BCCTarjanRec(const std::function<void(const std::vector<std::pair<std::string, std::string>>&)>& visitBCC, std::set<std::string>& articulationPoints, std::map<std::string, std::string>& bridges, int vertex, int & time, int * dfn, int * low, std::stack<std::pair<int, int>>& stack, int * parent, bool * isBridgesAlonePoint, bool is2EdgeConnected);


};
#include "BFS.h"
#include "DFS.h"
#include "GraphUtils.h"
#include "Kruskal.h"
#include "Prim.h"
#include "SCCKosaraju.h"
#include "SCCTarjan.h"
#include "BCCTarjan.h"
#include "ShortestPath.h"
