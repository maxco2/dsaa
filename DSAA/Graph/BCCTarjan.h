#pragma once
#include "Graph.h"
#include <map>

template<class T, class E>
inline void Graph<T, E>::BCCTarjan(std::function<void(const std::vector<std::pair<std::string,std::string>>&)> visitBCC, 
	std::set<std::string>& articulationPoints,
	std::map<std::string, std::string>& bridges,
	bool is2EdgeConnected)
{
	size_t vertexSize = vertices_.size();
	std::unique_ptr<int[]> dfn{ new int[vertexSize] };
	std::unique_ptr<int[]> low{ new int[vertexSize] };
	std::unique_ptr<int[]> parent{ new int[vertexSize] };
	std::unique_ptr<bool[]> isBridgesAlonePoint{ new bool[vertexSize] };
	for (size_t i = 0; i < vertexSize; ++i)
	{
		isBridgesAlonePoint[i] = true;
		dfn[i] = low[i] = -1;
		parent[i] = -1;
	}
	int time = 0;
	std::stack<std::pair<int,int>> stack;
	for (int vertex = 0; vertex < vertexSize; ++vertex)
	{
		if (dfn[vertex] == -1)
			BCCTarjanRec(visitBCC, 
				articulationPoints,
				bridges,
				vertex, 
				time, 
				dfn.get(), 
				low.get(), 
				stack,
				parent.get(),
				isBridgesAlonePoint.get(),
				is2EdgeConnected);
	}
	if (!stack.empty())
	{
		std::pair<int, int> uv{ 0,0 };
		std::vector<std::pair<std::string, std::string>> bcc;
		do
		{
			uv = stack.top();
			stack.pop();
			bcc.emplace_back(vertices_[uv.first].name, vertices_[uv.second].name);
		} while (!stack.empty());
		if (dfn[uv.first] < low[uv.second])
		{
			if(is2EdgeConnected)
				bcc.pop_back();
		}
		if (!bcc.empty())
			visitBCC(bcc);
	}
	if (is2EdgeConnected)
	{
		//add all alone points to 2-edge-cc
		std::vector<std::pair<std::string, std::string>> bcc;
		for (auto& i : bridges)
			if (isBridgesAlonePoint[getVertexByName(i.first)])
				bcc.emplace_back(i.first, "");
			else if(isBridgesAlonePoint[getVertexByName(i.second)])
				bcc.emplace_back(i.second, "");
		if (!bcc.empty())
			visitBCC(bcc);
	}
}

template<class T, class E>
inline void Graph<T, E>::BCCTarjanRec(const std::function<void(const std::vector<std::pair<std::string,std::string>>&)>& visitBCC,
	std::set<std::string>& articulationPoints,
	std::map<std::string,std::string>& bridges,
	int vertex, int & time,
	int * dfn, int * low,
	std::stack<std::pair<int,int>>& stack, 
	int* parent, 
	bool* isBridgesAlonePoint,
	bool is2EdgeConnected)
{
	dfn[vertex] = low[vertex] = ++time;
	int children = 0;
	bool popupEdges = false;
	Edge<E> *link = vertices_[vertex].link;
	while (link != nullptr)
	{
		if (dfn[link->dest] == -1)
		{
			children++;
			parent[link->dest] = vertex;
			// not visited
			stack.push({ vertex,link->dest });

			BCCTarjanRec(visitBCC, 
				articulationPoints,
				bridges,
				link->dest, 
				time, 
				dfn, 
				low, 
				stack,
				parent,
				isBridgesAlonePoint,
				is2EdgeConnected);

			if (low[link->dest] >= dfn[vertex])
			{
				if(parent[vertex]!=-1)
					articulationPoints.emplace(vertices_[vertex].name);
				if (!is2EdgeConnected)
					popupEdges = true;
				if (dfn[vertex] < low[link->dest])
				{
					bridges[vertices_[vertex].name] = vertices_[link->dest].name;
					if (is2EdgeConnected)
						popupEdges = true;
				}
			}
			low[vertex] = std::min(low[link->dest], low[vertex]);
			if ((parent[vertex] != -1 && popupEdges) ||
				(parent[vertex] == -1 && children > 1))
			{
				if(parent[vertex]==-1)//is root and ariculation point
					articulationPoints.emplace(vertices_[vertex].name);
				std::pair<int, int> uv{ 0,0 };
				std::vector<std::pair<std::string,std::string>> bcc;
				do
				{
					uv = stack.top();
					if (is2EdgeConnected)//memorize bridges not alone point 
						if (uv.first != vertex && uv.second != link->dest) 
						{
							isBridgesAlonePoint[uv.first] = false;
							isBridgesAlonePoint[uv.second] = false;
						}
					stack.pop();
					bcc.emplace_back( vertices_[uv.first].name,vertices_[uv.second].name );
				} while (!(uv.first==vertex&& uv.second==link->dest));
				if (dfn[uv.first] < low[uv.second])
				{
					if (is2EdgeConnected)
						bcc.pop_back();
				}
				if(!bcc.empty())
				visitBCC(bcc);
			}
		}
		else if (parent[vertex] != link->dest)
		{
			//parent[vertex] == link->dest indicates vertex--link->dest is not a back edge in which descendant points to parent is not a legal backedge.
			//dfn[link->dest] < dfn[vertex] is a legal back edge.
			//if dfn[link->dest] > defn[vertex] is a forward edge which vertex points to link->dest which is a descendant edge 
			//and shouldn't exist in an undirected graph
			if (dfn[link->dest] < dfn[vertex])
			{
				low[vertex] = std::min(dfn[link->dest], low[vertex]);
				stack.push({  vertex ,link->dest  });
			}
		}
		link = link->link;
	}
}