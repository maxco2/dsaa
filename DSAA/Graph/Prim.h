﻿#pragma once
#include "Graph.h"

template <typename T, typename E>
std::unordered_multimap<std::string, std::string>
Graph<T, E>::Prim(const std::string &startVertex)
{
    int pos = getVertexByName(startVertex);
    if (pos == -1) throw std::runtime_error{"wrong vertex name"};
    auto vertexSize = vertices_.size();
    std::unique_ptr<bool[]> isInMst{new bool[vertexSize]};
    for (int i = 0; i < vertexSize; ++i)
        {
			isInMst[i] = false;
        }
    struct SimpleEdge
    {
		int cost = std::numeric_limits<int>::max();
		int prior = -1;
        int vertexPos;
        SimpleEdge(int vertexPos, int cost=std::numeric_limits<int>::max(),int prior=-1)
            : vertexPos(vertexPos), cost(cost), prior(prior)
        {
        }
        bool operator<(const SimpleEdge &that)
        {
            return cost < that.cost;
        }
        bool operator>(const SimpleEdge &that)
        {
            return cost > that.cost;
        }
    };
    std::unordered_multimap<std::string, std::string> mst;
    Heap<SimpleEdge> heap{};
    SimpleEdge posVertex{pos, 0 };
    heap.insert(posVertex);
    while (!heap.empty() && mst.size() < vertexSize - 1)
        {
            SimpleEdge v = heap.remove();
			if (v.prior != -1 && !(isInMst[v.prior]&&isInMst[v.vertexPos]))//避免重复插入
                {
					isInMst[v.vertexPos] = true;
					isInMst[v.prior] = true;
                    if (v.prior < v.vertexPos)
                        mst.emplace(vertices_[v.prior].name,
                                    vertices_[v.vertexPos].name);
                    else
                        mst.emplace(vertices_[v.vertexPos].name,
                                    vertices_[v.prior].name);
                }
            Edge<E> *edge = vertices_[v.vertexPos].link;
            while (edge != nullptr)
                {
                    if (edge->dest != v.prior && //避免无向图边两次插入
						!isInMst[edge->dest])//避免生成树回路
                        {
							//heap 是以边cost为索引,所以可能会有重复点
							//但保证都是"桥"
						heap.insert(SimpleEdge{ edge->dest,
												 edge->cost,
												 v.vertexPos });
                        }
                    edge = edge->link;
                }
        }
    return mst;
}

