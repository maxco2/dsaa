#pragma once
#include "Graph.h"

template <typename T, typename E>
void Graph<T, E>::
Kosaraju(std::function<void(const std::vector<std::string> &)> visitSCC)
{
        if (vertices_.empty()) return;
        std::stack<int> stack;
        DFS({}, 0, [&stack](int v) { stack.push(v); });
        auto reverseGraph = std::unique_ptr<Graph<T, E>>{reverse()};
        while (!stack.empty())
            {
                auto topV = stack.top();
                stack.pop();
                if (reverseGraph->vertices_[topV].color == VertexColor::WHITE)
                    {
                        auto time = 0;
                        std::vector<std::string> names;
                        reverseGraph->DFSOneNode(
                            [&names](const std::string &name) {
                                names.push_back(name);
                            },
                            topV, time);
                        visitSCC(names);
                    }
            }
}