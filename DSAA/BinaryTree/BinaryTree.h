#pragma once
#include <functional>
#include <initializer_list>
#include <queue>
#include <stack>

template <class T>
struct BinaryTreeNode
{
    T data;
    BinaryTreeNode<T> *left = nullptr;
    BinaryTreeNode<T> *right = nullptr;
    int ltag = 0;
    int rtag = 0;

    // threaded tree op
    BinaryTreeNode<T> *first()
    {
        auto cur = this;
        while (cur->ltag == 0)
            cur = cur->left;
        return cur;
    }

    // threaded tree op
    BinaryTreeNode<T> *next()
    {
        auto cur = this->right;
        if (this->rtag == 0) return cur->first();
        return cur;
    }
    // threaded tree op
    BinaryTreeNode<T> *last()
    {
        auto cur = this;
        while (cur->rtag == 0)
            cur = cur->right;
        return cur;
    }

    // threaded tree op
    BinaryTreeNode<T> *prior()
    {
        auto cur = this->left;
        if (this->ltag == 0) return cur->last();
        return cur;
    }

    BinaryTreeNode() {}
    BinaryTreeNode(T x) : data(x) {}
};

template <class T>
class BinaryTree
{
public:
    using NodePtr = BinaryTreeNode<T> *;
    using Node = BinaryTreeNode<T>;

private:
    bool hadCreatedThread = false;

    BinaryTreeNode<T> *root = nullptr;
    void destroy(BinaryTreeNode<T> *subTree)
    {
        if (subTree == nullptr) return;
        destroy(subTree->left);
        destroy(subTree->right);
        delete subTree;
    }

    BinaryTreeNode<T> *insertNoAVL(BinaryTreeNode<T> *&subTree, const T &data)
    {
        if (subTree != nullptr)
            {
                if (subTree->data == data) return nullptr;
                if (data < subTree->data)
                    {
                        return insertNoAVL(subTree->left, data);
                    }
                return insertNoAVL(subTree->right, data);
            }
        subTree = new BinaryTreeNode<T>(data);
        subTree->data = data;
        return subTree;
    }

    NodePtr parent(NodePtr ancestor, NodePtr cur)
    {
        if (ancestor == nullptr) return nullptr;
        if (ancestor->left == cur || ancestor->right == cur) return ancestor;
        auto leftParent = parent(ancestor->left, cur);
        return leftParent ? leftParent : parent(ancestor->right, cur);
    }

    void preOrderNoRec(BinaryTreeNode<T> *subTree,
                       const std::function<void(T)> &visit)
    {
        struct Snap
        {
            BinaryTreeNode<T> *subTree;
            int stage = 0;
        };
        std::stack<Snap> s;
        Snap cur;
        cur.subTree = subTree;
        s.push(cur);
        while (!s.empty())
            {
                cur = s.top();
                s.pop();
                Snap snap;
                switch (cur.stage)
                    {
                    case 0:
                        if (cur.subTree == nullptr)
                            continue;
                        else
                            {
                                visit(cur.subTree->data);
                                cur.stage = 1;
                                s.push(cur);
                                snap.subTree = cur.subTree->left;
                                s.push(snap);
                            }
                        break;
                    case 1:
                        snap.subTree = cur.subTree->right;
                        s.push(snap);
                        break;
                    default:
                        break;
                    }
            }
    }

    void preOrder(BinaryTreeNode<T> *subTree,
                  const std::function<void(T)> &visit)
    {
        if (subTree == nullptr)
            return;
        else
            {
                visit(subTree->data);
                preOrder(subTree->left, visit);
                preOrder(subTree->right, visit);
            }
    }

    void inOrderNoRec(BinaryTreeNode<T> *subTree,
                      const std::function<void(T)> &visit)
    {
        struct Snap
        {
            BinaryTreeNode<T> *subTree;
            int stage = 0;
        };
        std::stack<Snap> s;
        Snap cur;
        cur.subTree = subTree;
        s.push(cur);
        while (!s.empty())
            {
                cur = s.top();
                s.pop();
                Snap snap;
                switch (cur.stage)
                    {
                    case 0:
                        if (cur.subTree == nullptr)
                            continue;
                        else
                            {
                                cur.stage = 1;
                                s.push(cur);
                                snap.subTree = cur.subTree->left;
                                s.push(snap);
                            }
                        break;
                    case 1:
                        visit(cur.subTree->data);
                        snap.subTree = cur.subTree->right;
                        s.push(snap);
                        continue;
                    default:
                        break;
                    }
            }
    }

    void inOrder(BinaryTreeNode<T> *subTree,
                 const std::function<void(T)> &visit)
    {
        if (subTree == nullptr)
            return;
        else
            {
                inOrder(subTree->left, visit);
                visit(subTree->data);
                inOrder(subTree->right, visit);
            }
    }

    void postOrderNoRec(BinaryTreeNode<T> *subTree,
                        const std::function<void(T)> &visit)
    {
        struct Snap
        {
            BinaryTreeNode<T> *subTree;
            int stage = 0;
        };
        std::stack<Snap> s;
        Snap cur;
        cur.subTree = subTree;
        s.push(cur);
        while (!s.empty())
            {
                cur = s.top();
                s.pop();
                Snap snap;
                switch (cur.stage)
                    {
                    case 0:
                        if (cur.subTree == nullptr)
                            continue;
                        else
                            {
                                cur.stage = 1;
                                s.push(cur);
                                snap.subTree = cur.subTree->left;
                                s.push(snap);
                            }
                        break;
                    case 1:
                        cur.stage = 2;
                        s.push(cur);
                        snap.subTree = cur.subTree->right;
                        s.push(snap);
                        break;
                    case 2:
                        visit(cur.subTree->data);
                        continue;
                    default:
                        break;
                    }
            }
    }

    void postOrder(BinaryTreeNode<T> *subTree,
                   const std::function<void(T)> &visit)
    {
        if (subTree == nullptr)
            return;
        else
            {
                postOrder(subTree->left, visit);
                postOrder(subTree->right, visit);
                visit(subTree->data);
            }
    }

    BinaryTreeNode<T> *find(BinaryTreeNode<T> *subTree, const T &data)
    {
        if (subTree == nullptr) return nullptr;
        if (subTree->data == data) return subTree;
        if (subTree->data < data) return find(subTree->right, data);
        return find(subTree->left, data);
    }

    /*
     * create threaded Tree
     */
    void createThread(BinaryTreeNode<T> *subTree, BinaryTreeNode<T> *&pre)
    {
        if (subTree == nullptr) return;
        createThread(subTree->left, pre);
        if (subTree->left == nullptr)
            {
                subTree->ltag = 1;
                subTree->left = pre;
            }
        if (pre != nullptr && pre->right == nullptr)
            {
                pre->rtag = 1;
                pre->right = subTree;
            }
        pre = subTree;
        createThread(subTree->right, pre);
    }

public:
    BinaryTree() {}

    BinaryTree(std::initializer_list<T> list)
    {
        for (auto &&i : list)
            {
                insertNoAVL(i);
            }
    }

    ~BinaryTree()
    {
        if (hadCreatedThread)
            {
                for (auto start = root->first(); start != nullptr;)
                    {
                        auto cur = start;
                        start = start->next();
                        delete cur;
                        cur = nullptr;
                    }
                return;
            }
        destroy(root);
        root = nullptr;
    }

    BinaryTreeNode<T> *find(const T &data) { return find(root, data); }

    BinaryTreeNode<T> *insertNoAVL(const T &data)
    {
        return insertNoAVL(root, data);
    }

    BinaryTreeNode<T> *parent(BinaryTreeNode<T> *cur)
    {
        if (root == cur || root == nullptr)
            return nullptr;
        else
            return parent(root, cur);
    }

    void preOrder(std::function<void(T)> visit) { preOrder(root, visit); }

    void inOrder(std::function<void(T)> visit) { inOrder(root, visit); }

    void inOrderThreaded(std::function<void(T)> visit)
    {
        for (auto start = root->first(); start != nullptr;
             start = start->next())
            visit(start->data);
    }

    void postOrder(std::function<void(T)> visit) { postOrder(root, visit); }

    void preOrderNoRec(std::function<void(T)> visit)
    {
        preOrderNoRec(root, visit);
    }

    void inOrderNoRec(std::function<void(T)> visit)
    {
        inOrderNoRec(root, visit);
    }

    void postOrderNoRec(std::function<void(T)> visit)
    {
        postOrderNoRec(root, visit);
    }

    void lev_order_traversal(std::function<void(T)> visit)
    {
        int cur = 0, last = 0;
        std::queue<BinaryTreeNode<T> *> q;
        q.push(root);
        while ((size_t)cur < q.size())
            {
                last = static_cast<int>(q.size());
                while (cur < last)
                    {
                        auto tmp = q.front();
						visit(tmp->data);
                        if (tmp->left != nullptr)
                            {
                                q.push(tmp->left);
                            }
                        if (tmp->right != nullptr)
                            {
                                q.push(tmp->right);
                            }
                        q.pop();
                        ++cur;
                    }
                cur = 0;
            }
    }

    void bfs(std::function<void(T)> visit)
    {
        if (root != nullptr)
            {
                BinaryTreeNode<T> *cur = root;
                std::queue<NodePtr> queue;
                queue.push(cur);
                while (!queue.empty())
                    {
                        cur = queue.front();
                        queue.pop();
						visit(cur->data);
                        if (cur->left) queue.push(cur->left);
                        if (cur->right) queue.push(cur->right);
                    }
            }
    }

    void createInThread()
    {
        if (root == nullptr) return;
        hadCreatedThread = true;
        BinaryTreeNode<T> *pre = nullptr;
        createThread(root, pre);
        if (pre != nullptr) pre->rtag = 1;
    }
};
